package exo8;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class RegistreJoueurs {
	private Map<Integer,List<String>> map = new HashMap<Integer,List<String>>();
	
	public RegistreJoueurs() {
	}

	public RegistreJoueurs(Map<Integer, List<String>> map) {
		this.map = map;
	}

	public void addJoueur(Joueur nouveau) {
		char[] annee = ("" + nouveau.getAnneeDeNaissance()).toCharArray();
		annee[3] = '0';
		String anneeString = new String (annee);
		int key = Integer.valueOf(anneeString);		
		//System.out.println("the key is " + key);
		
		if( this.map.get(key)== null) {
			List<String> nouveaux = new ArrayList<>();
			nouveaux.add(nouveau.getNom());
			map.put(key,nouveaux);
		} 
		else {
				List<String> nouveaux = map.get(key);
				nouveaux.add(nouveau.getNom());
		}
	}
	
	public List<String> get(int key) {
		if (!(map.get(key) == null)) {
			return map.get(key);			
		}else {
			return null;
		}
	}
	
	public int count(int key) {
		if (!(map.get(key) == null)) {
			return map.get(key).size();
		}else {
			return 0;
		}
	}
	
	public int count() {
		Set<Integer> KeySet = this.map.keySet();
		int sum = 0;
		for (int key: KeySet) {
			sum += map.get(key).size();
		}
		return sum;
	}

	@Override
	public String toString() {
		return "Player register :\n[map == " + map + "]";
	}
	
}
