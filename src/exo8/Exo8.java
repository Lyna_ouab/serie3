package exo8;

public class Exo8 {

	public static void main(String[] args) {
		RegistreJoueurs registre = new RegistreJoueurs();
		
		Joueur j1 = new Joueur("Laurent",1965);
		Joueur j2 = new Joueur("Marcel",1968);
		Joueur j3 = new Joueur("Patrik",1976);
		Joueur j4 = new Joueur("Alain",1970);
		Joueur j5 = new Joueur("Didier",1968);
		
		Joueur j6 = new Joueur("Steve",1980);
		Joueur j7 = new Joueur("Hugo",1986);
		Joueur j8 = new Joueur("Djibril",1992);
		Joueur j9 = new Joueur("Blaise",1987);
		Joueur j10 = new Joueur("paul",1993);

		registre.addJoueur(j1);
		registre.addJoueur(j2);
		registre.addJoueur(j3);
		registre.addJoueur(j4);
		registre.addJoueur(j5);
		
		System.out.println("The register contains " + registre.count() + " players");
		System.out.println(registre);
		
		registre.addJoueur(j6);
		registre.addJoueur(j7);
		registre.addJoueur(j8);
		registre.addJoueur(j9);
		registre.addJoueur(j10);
		
		System.out.println("\nNew players arrived!");
		System.out.println("The team get bigger,the register contains " + registre.count() + " players now!");
		System.out.println(registre);
		
		System.out.println("\nThe number of players born in the 60's:\t" + registre.count(1960));
		System.out.println("These players are :\t" + registre.get(1960) + "\n");
		System.out.println("The number of players born in the 70's:\t" + registre.count(1970));
		System.out.println("These players are :\t" + registre.get(1970) + "\n");
		System.out.println("The number of players born in the 80's:\t" + registre.count(1980));
		System.out.println("These players are :\t" + registre.get(1980) + "\n");
		System.out.println("The number of players born in the 90's:\t" + registre.count(1990));
		System.out.println("These players are :\t" + registre.get(1990) + "\n");
		
	}

}
