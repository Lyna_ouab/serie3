package exo7;

import java.util.ArrayList;
import java.util.Collection;

public class Equipe {
	Collection<Joueur> equipe = new ArrayList<>();
	
	public Equipe() {
	}

	public Equipe(Collection<Joueur> equipe) {
		this.equipe = equipe;
	}

	public void addJoueur(Joueur nouveau) {
		this.equipe.add(nouveau);
	}
	
	public boolean removeJoueur(Joueur nouveau) {
		return this.equipe.remove(nouveau);
	}
	
	public boolean isJoueurPresent(Joueur nouveau) {
		return this.equipe.contains(nouveau);
	}

	@Override
	public String toString() {
		//return "Equipe : " + this.equipe.size() + " joueurs \n" + this.equipe + "\n";
		System.out.println("Equipe : " + equipe.size() + " joueurs ");
		for(Joueur element : equipe) {
			System.out.println(element);
		}
		return null;
	}

	public void addAllEquipe(Equipe newEquipe) {
		newEquipe.equipe.removeAll(this.equipe);
		this.equipe.addAll(newEquipe.equipe);
	}
	
	public void clear() {
		this.equipe.clear();
	}

	public int getNombreJoueurs() {
		return this.equipe.size();
	}
	
	public double getMoyenneAge() {
		double sum = 0;
		for (Joueur element: equipe) {
			sum += element.getAge();
		}
		return sum/this.equipe.size();
	}
}
