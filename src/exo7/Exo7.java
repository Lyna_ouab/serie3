package exo7;

public class Exo7 {

	public static void main(String[] args) {
		Equipe MyTeam = new Equipe(); 
		Joueur joueur1 = new Joueur ("Mahrez",30);
		Joueur joueur2 = new Joueur ("Mbolhi",34);
		Joueur joueur3 = new Joueur ("Mandi",29);
		Joueur joueur4 = new Joueur ("Abeid",28);
		Joueur joueur5 = new Joueur ("Slimani",32);
		
		//System.out.println(joueur1.compareTo(joueur2)); // Returns 0 because they are equal
		
		MyTeam.addJoueur(joueur1); 
		MyTeam.addJoueur(joueur2); 
		MyTeam.addJoueur(joueur3); 
		MyTeam.addJoueur(joueur4); 
		MyTeam.addJoueur(joueur5); 
		System.out.println("Is " + joueur5.getNom() + " present ?\t" + MyTeam.isJoueurPresent(joueur5));
		MyTeam.toString(); 	
		
		System.out.println("\n\nRemoving " + joueur5.getNom()+ "\t" + MyTeam.removeJoueur(joueur5));
		System.out.println("Is " + joueur5.getNom() +" still present ?\t" + MyTeam.isJoueurPresent(joueur5));
		MyTeam.toString(); 	

		System.out.println("\n\n");
		EquipeLimitee newEquipe = new EquipeLimitee(3);
		newEquipe.addAllEquipe(MyTeam);
		newEquipe.toString();
		
		Equipe equipeSecondaire = new Equipe(); 
		Joueur player1 = new Joueur ("Mahrez",30);
		Joueur player2 = new Joueur ("Bennacer",23);
		Joueur player3 = new Joueur ("Slimani",32);
		Joueur player4 = new Joueur ("Atal",24);
		Joueur player5 = new Joueur ("Bensebaini",25);
		equipeSecondaire.addJoueur(player1); 
		equipeSecondaire.addJoueur(player2); 
		equipeSecondaire.addJoueur(player3); 
		equipeSecondaire.addJoueur(player4); 
		equipeSecondaire.addJoueur(player5); 
		//System.out.println("\nThe freshman!");
		//equipeSecondaire.toString();
		
		System.out.println("\nThe entire team :");
		MyTeam.addAllEquipe(equipeSecondaire);
		MyTeam.toString(); 	
	}
}
