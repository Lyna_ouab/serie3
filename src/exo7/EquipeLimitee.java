package exo7;

import java.util.ArrayList;
import java.util.Collection;

public class EquipeLimitee {
	Collection<Joueur> equipe = new ArrayList<>();
	int nombreMax;
	
	public EquipeLimitee(int nombreMax) {
		this.nombreMax = nombreMax;
	}

	public void addJoueur(Joueur nouveau) {
		if (equipe.size() < nombreMax) {
			this.equipe.add(nouveau);
		}
		else {
			System.out.println("This team reached " + nombreMax + " players! There are no more posts left");
		}
	}
	
	public boolean removeJoueur(Joueur nouveau) {
		return this.equipe.remove(nouveau);
	}
	
	public boolean isJoueurPresent(Joueur nouveau) {
		return this.equipe.contains(nouveau);
	}
	
	@Override
	public String toString() {
		System.out.println("Equipe : " + equipe.size() + " joueurs ");
		for(Joueur element : equipe) {
			System.out.println(element.toString());
		}
		return null;
	}

	public void addAllEquipe(Equipe newEquipe) {
		for (Joueur element: newEquipe.equipe) {
			if (!(this.isJoueurPresent(element))) {
				this.addJoueur(element);
			}
		}
			
	}
	
	public void clear() {
		this.equipe.clear();
	}

	public int getNombreJoueurs() {
		return this.equipe.size();
	}
	
	public double getMoyenneAge() {
		double sum = 0;
		for (Joueur element: equipe) {
			sum += element.getAge();
		}
		return sum/this.equipe.size();
	}
}
